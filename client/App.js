import './App.css'
import Routing from './Routes'
import NavDrawer from './components/Navigation/NavDrawer'

function App() {
  return (
    <div className="App">
      <NavDrawer />
      <Routing />
    </div>
  )
}

export default App
