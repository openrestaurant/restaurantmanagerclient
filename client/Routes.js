import { Routes, Route } from 'react-router-dom'
import OrdersPage from './pages/OrdersPage'
import DeliveriesPage from './pages/DeliveriesPage'
import MenusPage from './pages/MenusPage'
import ClientsPage from './pages/ClientsPage'
import DriversPage from './pages/DriversPage'
import SettingsPage from './pages/SettingsPage'

function Routing() {
  return (
    <Routes>
      <Route path={'/'} element={<OrdersPage />} />
      <Route path="orders" element={<OrdersPage />} />
      <Route path="deliveries" element={<DeliveriesPage />} />
      <Route path="menus" element={<MenusPage />} />
      <Route path="clients" element={<ClientsPage />} />
      <Route path="drivers" element={<DriversPage />} />
      <Route path="settings" element={<SettingsPage />} />
    </Routes>
  )
}

export default Routing
