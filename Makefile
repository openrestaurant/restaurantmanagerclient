

init:
	npm ci

format:
	eslint . --fix

start:
	npm start

.PHONY: start
